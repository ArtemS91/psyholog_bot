const { Telegraf, Scenes, session, Input, Markup } = require("telegraf");
const { CronJob } = require("cron");

// async function StepOfBot(ctx, message, answer, sticker) {
//   try {
//     if (ctx.message.text.length < 5) {
//       await ctx.reply(
//         "Розкрийся більше для себе, подумай як ти можеш описати це краще"
//       );
//     } else {
//       if (answer.length > 1) {
//         await ctx.reply(answer);
//       }
//       ////"00 01 * * *"
//       const senderOfMessage = CronJob.from({
//         cronTime: "00 01 * * *",
//         onTick: async () => {
//           try {
//             senderOfMessage.start();
//             await ctx.reply(sticker);
//             await ctx.reply(message);
//             return senderOfMessage.stop();
//           } catch (error) {
//             bot.telegram.sendMessage(
//               630678819,
//               "ошибка на відправленні повідомлення у кроні"
//             );
//           }
//         },
//         start: true,
//       });
//       return ctx.wizard.next();
//     }
//   } catch (error) {}
// }

// function linkToDasha() {
//   return Markup.inlineKeyboard(
//     [Markup.button.url("Зв'язатися зі мною", "https://t.me/DaraPidd")],
//     {
//       columns: 1,
//     }
//   );
// }

const firstSticker = `🐣`;
const secondSticker = `🔎`;
const thirdSticker = `🫂`;
const foreSticker = `👁️`;
const fiveSticker = `🎨`;
const sixeSticker = `🧘🏻`;
const sevenSticker = `🪬`;
const eightSticker = `👨‍👩‍👧‍👦`;
const nineSticker = `🤭`;
const tenSticker = `⚡️`;
const elevenSticker = `📝`;
const twelfSticker = `🤌🏼`;
const thirteenSticker = `⏳`;
const foreteenSticker = `🔋`;

const firstText = `День 1: Початок.\n\nНовий початок твого шляху вдячності.\nЗапрошую тебе пригадати три речі, за які ти вдячний(-на) у своєму житті.\n\n🪄Практика: Запиши ці три речі в свій щоденник вдячності\n\n💭Вдячність - це вища форма сили.`;
const secondText = `День 2: Дослідження.\n\nЗроби собі подарунок - вийди на свіже повітря ( вигляни з вікна чи вийди на балкон) та занурся в повітря та природу.\n\n🪄Практика: Розкажи у щоденнику про свої відчуття від ранку на природі та зафіксуй три речі, які зацікавили тебе\n\n💭Природа завжди має щось, за що можна бути вдячним.`;
const thirdText = `День 3: Люди навколо.\n\nСьогодні день вдячності за твою спільноту. Подякуй тим, хто завжди поруч.\n\n🪄Практика: Запиши троє людей, яким ти вдячний(-на), та поділись з ними своїми почуттями\n\n💭"Справжня вдячність - це ділитися своєю вдячністю з іншими.`;
const foreText = `День 4: Тіло.\n\nСьогодні подякуй своєму тілу за все, що воно робить для тебе.\n\n🪄Практика: Запиши три риси свого тіла, за які ти вдячний(-на)\n\n💭Твоє тіло - твій найкращий союзник у житті. Будь вдячний за нього.`;
const fiveText = `День 5: Творчість.\n\nСьогодні зосередься на своїх творчих здібностях. Напиши за які таланти ти вдячний(-на), що вони приносять тобі?\n\n🪄Практика: Спробуй щось нове сьогодні, щоб відкрити свою творчу енергію.\n\n💭Творчість - це виявлення вдячності за прекрасне у світі.`;
const sixeText = `День 6: Присутність.\n\nСьогодні візьми на озброєння філософію 'тут і зараз'. Подякуй за кожен момент присутності.\n\n🪄Практика: Запиши три миті, які зачепили тебе сьогодні. Не поспішай.\n\n💭Життя складається з митей. Вдячність робить їх неповторними.💭`;
const sevenText = `День 7: Душа.\n\nСьогодні дай своїй душі відпочинок. Роби те, що тебе радує.\n\n🪄Практика: Напиши вдячність собі за те, що вмієш відпочивати та бережний з собою.\n\n💭Душа також потребує часу для відпочинку. Будь вдячний за моменти самопідтримки.`;
const eightText = `День 8: Родина.\n\nСьогодні день вдячності за твою сім'ю та твій могутній рід. Які моменти з ними тобі особливо запам'ятовуються? Кому з роду ти можеш подякувати за твої знання та вміння?\n\n🪄Практика: Зателефонуй або напиши своїм близьким та подякуй їм за те, що вони є у твоєму житті або/та лягаючи спати, закрий очі та уяви свій могутній рід. Уяви, як ви танцюєте в колі, сильно тримаючись за руки, уяви яскраве багаття всередині вашого кола, заглянь своєму роду в очі. Скажи Дякую.\n💭Рід - це де ми навчаємося кохати і бути коханими, де завжди є місце для нас, незалежно від того, який шлях ми обираємо.`;
const nineText = `День 9: Сміх.\n\nСьогодні фокусуйся на сміху та радості. Які речі роблять тебе по-справжньому щасливим(-ою)?\n\n🪄Практика: Запиши три речі, що викликали у тебе сміх або радість сьогодні.\n\n💭Сміх - це мова вдячності душі. Насолоджуйся моментами радості.`;
const tenText = `День 10: Здоров'я і Енергія.\n\nСьогодні подякуй своєму тілу за здоров'я і енергію, яке воно тобі дарує.\n\n🪄Практика: Запиши три способи, якими ти можеш зберігати своє здоров'я та підтримувати енергію.\n\n💭Здоров'я - це найбільший скарб. Будь вдячний за своє тіло і дбай про нього.`;
const elevenText = `День 11: Можливості.\n\nСьогодні роздумуй про всі можливості, які надає тобі життя. За що ти вдячний(-на) сьогодні?\n\n🪄Практика: Запиши три можливості, які з'явилися у тебе нещодавно та які ти використовуєш.\n\n💭Майбутнє належить тим, хто вірить у красу своїх мрій. Будь вдячний за кожну можливість.`;
const twelfText = `День 12: Зосередженість.\n\nСьогодні роби все свідомо та уважно. Запам'ятай кожен момент.\n\n🪄Практика: Спробуйте медитацію або вправи зосередженості та запиши свої враження.\n\n💭Сьогодні - подарунок. Зосередься на ньому і будь вдячний(-на) за цей час.`;
const thirteenText = `День 13: Зміни.\n\nСьогодні зосередься на своїх мріях та цілях. Які кроки ти можеш зробити сьогодні, щоб наблизитися до них?\n\n🪄Практика: Запиши три речі, які ти можеш зробити, щоб реалізувати свої мрії.\n\n💭Майбутнє залежить від того, що ти робиш сьогодні. Будь вдячний(-на) за можливість будувати своє життя.`;
const foreteenText = `День 14: Завершення.\n\nСьогодні останній день твого вдячності. Подякуй собі за те, що ти витримав(-ла) цей виклик.\n\n🪄Практика: Підведи підсумок своїх вражень від 14 днів вдячності. Що ти вивчив(-ла) про себе за цей час?\n\n💭Вдячність - це не тільки стан душі, але й здатність бачити прекрасне у кожному дні.`;

// const firstAnsweer = `Так тримати! Ти вже на шляху своєї трансформації.`;
// const secondAnsweer = `Твоє бажання вивчати нові речі вражає. Продовжуй пірнати глибше в себе, ти робиш це чудово!`;
// const thirdAnsweer = `Твоя здатність з'єднувати людей вражає. Дякую, що створюєш тепло навколо себе!`;
// const foreAnsweer = `Твоя увага до власного тіла і здоров'я - безцінна. Тримайся свого союзника, прислуховуйся, довіряй`;
// const fiveAnsweer = `Ти виявляєш свою творчість і креативність. Це неймовірно! Продовжуй надихати інших своєю енергією, проявляй свій потенціал!`;
// const sixeAnsweer = `Ти пронизуєш позитивом, світлом та добром. Це так потрібно у цьому світі. Дякую за твою світлу особистість!`;
// const sevenAnsweer = `Це так класно, що ти приділяєш увагу своєму відпочинку. Якісний відпочинок - твоя запорука благополуччя.`;
// const eightAnsweer = `Твоя сім'я - твоя сила. Щира вдячність за Рід, що завжди стоїть за тобою, навіть коли весь світ проти тебе.`;
// const nineAnsweer = `Твій сміх - справжня магія! Не забувай регулярно радіти життю і ділитися цією енергією з іншими.`;
// const tenAnsweer = `Ти показуєш, що здоров'я - це скарб. Продовжуй дбати про себе і вдячність за твоє здоров'я буде тебе надихати.`;
// const elevenAnsweer = `Твої мрії - твоя сила. Продовжуй вірити в них наче вони вже тут з вами!`;
// const twelfAnsweer = `Твоя гармонія надихає. Продовжуй відслідковувати цей шлях вдячності і гармонії, ти робиш це чудово!`;
// const thirteenAnsweer = `Коли ви не можете змінити напрямок вітру, змініть своє вітрило. Дякую, що ти так відкрита/ий для змін!`;
// const foreteenAnsweer = `Ти пройшов великий шлях внутрішньої трансформації, підтягуючи зовнішнє. Не знецінюй, не забувай. Дякую, за створення сильної енергії у цьому челенджі.\n\nТи надихаєш.`;

const superWizard = new Scenes.WizardScene("super-wizard", (ctx) => {
  const senderOfMessage = CronJob.from({
    cronTime: "00 00 * * *",
    onTick: async () => {
      try {
        await ctx.reply(firstSticker);
        await ctx.reply(firstText);
        senderOfMessage.stop();
        ctx.scene.enter("super-wizard2");
        //
      } catch (error) {
        console.log(error);
        bot.telegram.sendMessage(
          630678819,
          "ошибка на відправленні повідомлення у кроні"
        );
      }
    },
    start: true,
  });
  return ctx.scene.leave();
});

const superWizard2 = new Scenes.WizardScene("super-wizard2", (ctx) => {
  const senderOfMessage = CronJob.from({
    cronTime: "00 00 * * *",
    onTick: async () => {
      try {
        await ctx.reply(secondSticker);
        await ctx.reply(secondText);
        senderOfMessage.stop();
        ctx.scene.enter("super-wizard3");
        //
      } catch (error) {
        console.log(error);
        bot.telegram.sendMessage(
          630678819,
          "ошибка на відправленні повідомлення у кроні"
        );
      }
    },
    start: true,
  });
  return ctx.scene.leave();
});

const superWizard3 = new Scenes.WizardScene("super-wizard3", (ctx) => {
  const senderOfMessage = CronJob.from({
    cronTime: "00 00 * * *",
    onTick: async () => {
      try {
        await ctx.reply(thirdSticker);
        await ctx.reply(thirdText);
        senderOfMessage.stop();
        ctx.scene.enter("super-wizard4");
        //
      } catch (error) {
        console.log(error);
        bot.telegram.sendMessage(
          630678819,
          "ошибка на відправленні повідомлення у кроні"
        );
      }
    },
    start: true,
  });
  return ctx.scene.leave();
});

const superWizard4 = new Scenes.WizardScene("super-wizard4", (ctx) => {
  const senderOfMessage = CronJob.from({
    cronTime: "00 00 * * *",
    onTick: async () => {
      try {
        await ctx.reply(foreSticker);
        await ctx.reply(foreText);
        senderOfMessage.stop();
        ctx.scene.enter("super-wizard5");
        //
      } catch (error) {
        console.log(error);
        bot.telegram.sendMessage(
          630678819,
          "ошибка на відправленні повідомлення у кроні"
        );
      }
    },
    start: true,
  });
  return ctx.scene.leave();
});

const superWizard5 = new Scenes.WizardScene("super-wizard5", (ctx) => {
  const senderOfMessage = CronJob.from({
    cronTime: "00 00 * * *",
    onTick: async () => {
      try {
        await ctx.reply(fiveSticker);
        await ctx.reply(fiveText);
        senderOfMessage.stop();
        ctx.scene.enter("super-wizard6");
      } catch (error) {
        console.log(error);
        bot.telegram.sendMessage(
          630678819,
          "ошибка на відправленні повідомлення у кроні"
        );
      }
    },
    start: true,
  });
  return ctx.scene.leave();
});

const superWizard6 = new Scenes.WizardScene("super-wizard6", (ctx) => {
  const senderOfMessage = CronJob.from({
    cronTime: "00 00 * * *",
    onTick: async () => {
      try {
        await ctx.reply(sixeSticker);
        await ctx.reply(sixeText);
        senderOfMessage.stop();
        ctx.scene.enter("super-wizard7");
      } catch (error) {
        console.log(error);
        bot.telegram.sendMessage(
          630678819,
          "ошибка на відправленні повідомлення у кроні"
        );
      }
    },
    start: true,
  });
  return ctx.scene.leave();
});

const superWizard7 = new Scenes.WizardScene("super-wizard7", (ctx) => {
  const senderOfMessage = CronJob.from({
    cronTime: "00 00 * * *",
    onTick: async () => {
      try {
        await ctx.reply(sevenSticker);
        await ctx.reply(sevenText);
        senderOfMessage.stop();
        ctx.scene.enter("super-wizard8");
      } catch (error) {
        console.log(error);
        bot.telegram.sendMessage(
          630678819,
          "ошибка на відправленні повідомлення у кроні"
        );
      }
    },
    start: true,
  });
  return ctx.scene.leave();
});

const superWizard8 = new Scenes.WizardScene("super-wizard8", (ctx) => {
  const senderOfMessage = CronJob.from({
    cronTime: "00 00 * * *",
    onTick: async () => {
      try {
        await ctx.reply(eightSticker);
        await ctx.reply(eightText);
        senderOfMessage.stop();
        ctx.scene.enter("super-wizard9");
      } catch (error) {
        console.log(error);
        bot.telegram.sendMessage(
          630678819,
          "ошибка на відправленні повідомлення у кроні"
        );
      }
    },
    start: true,
  });
  return ctx.scene.leave();
});

const superWizard9 = new Scenes.WizardScene("super-wizard9", (ctx) => {
  const senderOfMessage = CronJob.from({
    cronTime: "00 00 * * *",
    onTick: async () => {
      try {
        await ctx.reply(nineSticker);
        await ctx.reply(nineText);
        senderOfMessage.stop();
        ctx.scene.enter("super-wizard10");
      } catch (error) {
        console.log(error);
        bot.telegram.sendMessage(
          630678819,
          "ошибка на відправленні повідомлення у кроні"
        );
      }
    },
    start: true,
  });
  return ctx.scene.leave();
});

const superWizard10 = new Scenes.WizardScene("super-wizard10", (ctx) => {
  const senderOfMessage = CronJob.from({
    cronTime: "00 00 * * *",
    onTick: async () => {
      try {
        await ctx.reply(tenSticker);
        await ctx.reply(tenText);
        senderOfMessage.stop();
        ctx.scene.enter("super-wizard11");
      } catch (error) {
        console.log(error);
        bot.telegram.sendMessage(
          630678819,
          "ошибка на відправленні повідомлення у кроні"
        );
      }
    },
    start: true,
  });
  return ctx.scene.leave();
});

const superWizard11 = new Scenes.WizardScene("super-wizard11", (ctx) => {
  const senderOfMessage = CronJob.from({
    cronTime: "00 00 * * *",
    onTick: async () => {
      try {
        await ctx.reply(elevenSticker);
        await ctx.reply(elevenText);
        senderOfMessage.stop();
        ctx.scene.enter("super-wizard12");
      } catch (error) {
        console.log(error);
        bot.telegram.sendMessage(
          630678819,
          "ошибка на відправленні повідомлення у кроні"
        );
      }
    },
    start: true,
  });
  return ctx.scene.leave();
});

const superWizard12 = new Scenes.WizardScene("super-wizard12", (ctx) => {
  const senderOfMessage = CronJob.from({
    cronTime: "00 00 * * *",
    onTick: async () => {
      try {
        await ctx.reply(twelfSticker);
        await ctx.reply(twelfText);
        senderOfMessage.stop();
        ctx.scene.enter("super-wizard13");
      } catch (error) {
        console.log(error);
        bot.telegram.sendMessage(
          630678819,
          "ошибка на відправленні повідомлення у кроні"
        );
      }
    },
    start: true,
  });
  return ctx.scene.leave();
});

const superWizard13 = new Scenes.WizardScene("super-wizard13", (ctx) => {
  const senderOfMessage = CronJob.from({
    cronTime: "00 00 * * *",
    onTick: async () => {
      try {
        await ctx.reply(thirteenSticker);
        await ctx.reply(thirteenText);
        senderOfMessage.stop();
        ctx.scene.enter("super-wizard14");
      } catch (error) {
        console.log(error);
        bot.telegram.sendMessage(
          630678819,
          "ошибка на відправленні повідомлення у кроні"
        );
      }
    },
    start: true,
  });
  return ctx.scene.leave();
});

const superWizard14 = new Scenes.WizardScene("super-wizard14", (ctx) => {
  const senderOfMessage = CronJob.from({
    cronTime: "00 00 * * *",
    onTick: async () => {
      try {
        await ctx.reply(foreteenSticker);
        await ctx.reply(foreteenText);
        senderOfMessage.stop();
        //
      } catch (error) {
        console.log(error);
        bot.telegram.sendMessage(
          630678819,
          "ошибка на відправленні повідомлення у кроні"
        );
      }
    },
    start: true,
  });
  return ctx.scene.leave();
});

const superWizard15 = new Scenes.WizardScene("super-wizard15", (ctx) => {
  const senderOfMessage = CronJob.from({
    cronTime: "00 03 * * *",
    onTick: async () => {
      try {
        await ctx.reply(
          `Дорогий учаснику,\n\nЦе завершення твого челенджу в "Фабриці Вдячності", але не завершення твоєї трансформації.\n\nТи проявляєш величезний ентузіазм і готовність розвиватися кожен день.Тож нехай цей челендж буде для тебе ще одним новим початком.\n\nБудь провідником для самого себе. Пам'ятай кожен твій прояв в цей світ - безпечний.\n\nОбійми!\n\nКоманда "Фабрика Вдячності.`
        );
        senderOfMessage.stop();
        //
      } catch (error) {
        console.log(error);
        bot.telegram.sendMessage(
          630678819,
          "ошибка на відправленні повідомлення у кроні"
        );
      }
    },
    start: true,
  });
  return ctx.scene.leave();
});

// const superWizard = new Scenes.WizardScene(
//   "super-wizard",
//   async (ctx) => await StepOfBot(ctx, firstText, "", firstSticker),
//   async (ctx) => await StepOfBot(ctx, secondText, firstAnsweer, secondSticker),
//   async (ctx) => await StepOfBot(ctx, thirdText, secondAnsweer, thirdSticker),
//   async (ctx) => await StepOfBot(ctx, foreText, thirdAnsweer, foreSticker),
//   async (ctx) => await StepOfBot(ctx, fiveText, foreAnsweer, fiveSticker),
//   async (ctx) => await StepOfBot(ctx, sixeText, fiveAnsweer, sixeSticker),
//   async (ctx) => await StepOfBot(ctx, sevenText, sixeAnsweer, sevenSticker),
//   async (ctx) => await StepOfBot(ctx, eightText, sevenAnsweer, eightSticker),
//   async (ctx) => await StepOfBot(ctx, nineText, eightAnsweer, nineSticker),
//   async (ctx) => await StepOfBot(ctx, tenText, nineAnsweer, tenSticker),
//   async (ctx) => await StepOfBot(ctx, elevenText, tenAnsweer, elevenSticker),
//   async (ctx) => await StepOfBot(ctx, twelfText, elevenAnsweer, twelfSticker),
//   async (ctx) => await StepOfBot(ctx, thirteenText, twelfAnsweer, thirteenSticker),
//   async (ctx) =>
//     await StepOfBot(ctx, foreteenText, thirteenAnsweer, foreteenSticker),
//   async (ctx) => {
//     try {
//       if (ctx.message.text.length < 5) {
//         await ctx.reply(
//           "Розкрийся більше для себе, подумай як ти можеш описати це краще"
//         );
//       } else {
//         await ctx.reply(foreteenAnsweer);
//         await ctx.scene.leave();
//         setTimeout(async () => {
//           await ctx.reply(
//             `Дорогий учаснику,\n\nЦе завершення твого челенджу в "Фабриці Вдячності", але не завершення твоєї трансформації.\n\nТи проявляєш величезний ентузіазм і готовність розвиватися кожен день.Тож нехай цей челендж буде для тебе ще одним новим початком.\n\nБудь провідником для самого себе. Пам'ятай кожен твій прояв в цей світ - безпечний.\n\nОбійми!\n\nКоманда "Фабрика Вдячності.`
//           );
//         }, 30000);

//         return;
//       }
//     } catch (error) {}
//   }
// );
////7040424239:AAF2M9Rvx0-NEMs6F58uRyNq4apn54GPfxM
/////7192517242:AAFsLBqsP1o7-ESOp7wrZyCqSCypya1WcgQ

const bot = new Telegraf("7040424239:AAF2M9Rvx0-NEMs6F58uRyNq4apn54GPfxM");
const stage = new Scenes.Stage([
  superWizard,
  superWizard2,
  superWizard3,
  superWizard4,
  superWizard5,
  superWizard6,
  superWizard7,
  superWizard8,
  superWizard9,
  superWizard10,
  superWizard11,
  superWizard12,
  superWizard13,
  superWizard14,
  superWizard15,
]);
bot.use(session());
bot.use(stage.middleware());
bot.start(async (ctx) => {
  await ctx.reply(
    `Ласкаво просимо на "YouFactory: Фабрику Вдячності" – тут ти – митець власного щастя.\nКожен день – це можливість перетворити буденність у щасливі миті.\n\nТи - інженер цієї фабрики.\nЛюбов і світло тобі завжди в асистенти. 14 днів твоєї щоденної практики починаються завтра.\n\nПиши мені сміливо все, що відчуваєш. Це тільки між нами.\n\n`
  );
  await ctx.reply(
    "Кожен наступний день о 8 ранку ти будеш отримувати нове завдання.\n\nДо зустрічі завтра 💫🫂"
  );
  return await ctx.scene.enter("super-wizard");
});

// bot.on("text", async (ctx) => {
//   try {
//     await ctx.reply(
//       `Якщо залишись питання чи потрібна допомога, пиши мені в особисте. Тисни кнопку "Зв'язатися зі мною`,
//       linkToDasha()
//     );
//   } catch (error) {
//     console.log("Помилка в повыдомленны на будь який текст ");
//   }
// });

bot.launch();
